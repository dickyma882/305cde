var express = require('express');
var app = express();
var http = require('http').Server(app);
var port = process.env.POST || 8080;
var session = require('express-session');
var request = require('request');
var bodyParser = require('body-parser');

var config = {
    apiKey: "AIzaSyDYzJlIC7nB8jRAsgHPM5G2zPg0EphOlPI",
    authDomain: "cde-91ba6.firebaseapp.com",
    databaseURL: "https://cde-91ba6.firebaseio.com",
    projectId: "cde-91ba6",
    storageBucket: "cde-91ba6.appspot.com",
    messagingSenderId: "798444885004"
};

var firebase = require('firebase');

firebase.initializeApp(config);
var db = firebase.database();

app.use(express.urlencoded({
    extended: true
}));

app.use(session({
    secret: 'abc',
    resave: true,
    saveUninitialized: true
}));

app.set('views', __dirname + '/');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(express.static(__dirname + '/css'));
app.use(express.static(__dirname + '/images'));
app.use(express.static(__dirname + '/dist'));



app.get('/', function(req, res) {
    res.set('Content-Type', 'text/html');
    if (req.session.login == "1") {
        res.render("home.html", { msg: '' });
    }
    else {
        res.render("index.html", { msg: '' });
    }
    res.end();
});


app.get('/home', function(req, res) {
    res.set('Content-Type', 'text/html');
    if (req.session.login == "1") {
        res.render("home.html", { msg: '' });
    }
    else {
        res.render("index.html", { msg: '' });
    }
    res.end();
});

app.get('/update/:city', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)
    request.put("https://assigment882.herokuapp.com/city/" + requestCity.city, function(error, res2, body) {
        if (res.statusCode == 200) {
            console.log("Work Fine");
        }
        var systemDateTime = new Date();
        console.log("update suss");
        console.log(systemDateTime);
        res.render("home.html", { msg: 'updatesuss' });
        res.end();
    })
});

app.get('/delete/:city', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)
    request.delete("https://assigment882.herokuapp.com/city/" + requestCity.city, function(error, res2, body) {
        if (res.statusCode == 200) {
            console.log("Work Fine");
        }
        console.log("del suss");
        res.render("home.html", { msg: 'deletesuss' });
        res.end();
    })
});

app.post('/add', function(req, res) {
    var city = req.body.city;
    console.log(city);
    request.post("https://assigment882.herokuapp.com/city/" + city, function(error, res2, body) {
        if (res.statusCode == 200) {
            console.log("Work Fine");
        }
        console.log("add suss");
        res.render("home.html", { msg: 'addsuss' });
        res.end();
    })
});

app.post('/register', function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var name = req.body.name;
    var email = req.body.email;
    var age = req.body.age;
    var ref = db.ref("/accounts/" + username);
    var value = {
        username: username + "",
        password: password + "",
        name: name + "",
        email: email + "",
        age: age
    }
    ref.set(value);
    res.render("index.html", { msg: 'registersuss' });
});

app.post('/login', function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var pass = "0";
    db.ref().child('accounts').once('value', function(snapshot) {
        snapshot.forEach(function(accounts) {
            if (accounts.val().username == username && accounts.val().password == password) {
                req.session.login = "1";
                res.render("home.html", { msg: 'loginsuss' });
                res.end();
                pass = "1";
                console.log("logined");
            }
        });
        if (pass == "0"){
             res.render("index.html", { msg: 'loginfail' });
        }
        console.log(pass);
    });
});


app.get('/forecast/:city', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)
    request.get("https://assigment882.herokuapp.com/forecast/" + requestCity.city, function(error, res2, body) {
        res.send(body)
        res.end();
    })
});

app.get('/getall', function(req, res) {
    request.get("https://assigment882.herokuapp.com/city", function(error, res2, body) {
        res.send(body)
        res.end();
    })
});

app.get('/get/:city', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)
    request.get("https://assigment882.herokuapp.com/city/" + requestCity.city, function(error, res2, body) {
        res.send(body)
        res.end();
    })
});

app.get('/signin', function(req, res, html) {
    res.render("signin.html");
    res.end();
});

app.get('/goLogin', function(req, res, html) {
    res.render("index.html", { msg: '' });
    res.end();
});

app.get('/logout', function(req, res, html) {
    req.session.destroy();
    res.render("index.html", { msg: '' });
    res.end();
});


http.listen(port, function() {
    console.log('Server Port : ' + port);
});
