var restify = require('restify');
var request = require('request');
const server = restify.createServer({
    name: 'myapp',
    version: '1.0.0'
});

var addData = require('./addData.js');
var updateData = require('./updateData.js');
var deleteData = require('./deleteData.js');
var showData = require('./showData.js');
var firebase = require("firebase");

//new
var express        = require( 'express' );
var http           = require( 'http' );

var app            = express();
app.set( 'port', process.env.PORT || 3001 );
app.get('/', function (req, res) {
    res.send('Hello World');
});

http.createServer( app ).listen( app.get( 'port' ), function (){
  console.log( 'Express server listening on port ' + app.get( 'port' ));
});



var config = {
    apiKey: "AIzaSyDYzJlIC7nB8jRAsgHPM5G2zPg0EphOlPI",
    authDomain: "cde-91ba6.firebaseapp.com",
    databaseURL: "https://cde-91ba6.firebaseio.com",
    projectId: "cde-91ba6",
    storageBucket: "cde-91ba6.appspot.com",
    messagingSenderId: "798444885004"
};

firebase.initializeApp(config);
var db = firebase.database();

app.use(restify.plugins.acceptParser(server.acceptable));
app.use(restify.plugins.queryParser());
app.use(restify.plugins.bodyParser());

app.post('/city/:name', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)

    request.get("http://api.openweathermap.org/data/2.5/weather?q=" + requestCity.name + "&appid=d000ff0bd04ff744cf2afc63c4c88cde&units=metric", function(error, res2, body) {
        if (res.statusCode == 200) {
            console.log("Work Fine");
        }
        console.log("add suss");    
        addData(body, db, titleCase(requestCity.name));
        res.end();
    })
});

app.get('/city/:name', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)
    showData(db, titleCase(requestCity.name),res);
    console.log("show suss");    
});

// get forecast 
app.get('/forecast/:name', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)
    request.get("http://api.openweathermap.org/data/2.5/forecast?q="+requestCity.name+"&appid=d000ff0bd04ff744cf2afc63c4c88cde&units=metric", function(error, res2, body) {
        if (res.statusCode == 200) {
            console.log("Work Fine");
        }
        console.log("show suss");
        res.send(body);
        res.end();
    })    
});

app.get('/city', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)
    db.ref("/city/").once("value").then(function(snapshot) {
         res.send(snapshot.val());
         res.end();
    });
    console.log("show suss"); 
});

app.del('/city/:name', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)
    deleteData(db, titleCase(requestCity.name));
    res.end();
});

app.put('/city/:name', function(req, res) {
    var reqJSON = JSON.stringify(req.params)
    var requestCity = JSON.parse(reqJSON)
     request.get("http://api.openweathermap.org/data/2.5/weather?q=" + requestCity.name + "&appid=d000ff0bd04ff744cf2afc63c4c88cde&units=metric", function(error, res2, body) {
        if (res.statusCode == 200) {
            console.log("Work Fine");
        }
        console.log("update suss");    
        updateData(body, db, titleCase(requestCity.name));
        res.end();
    })
    res.end();
});

function titleCase(str) {
    return str.replace(/\w\S*/g, function(word) {
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    });
}

app.listen(8080, function() {
    console.log('%s listening at %s', app.name, app.url);
});