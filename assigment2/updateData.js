module.exports = function(body, db, city) {
    var obj = JSON.parse(body);
    var temp = obj.main.temp;
    var systemDateTime = new Date();
    var y = systemDateTime.getFullYear();
    var m = systemDateTime.getMonth()+1;
    var d = systemDateTime.getDate();
    var date = y+"-"+m+"-"+d;
    var humidity = obj.main.humidity;
    var temp_min = obj.main.temp_min;
    var temp_max = obj.main.temp_max;
    var ref = db.ref("/city/" + city);
    var value = {
        city: city+"",
        temp: temp + "",
        humidity: humidity+"",
        temp_min: temp_min+"",
        temp_max: temp_max+"",
        date: date
    }
    ref.update(value);
}