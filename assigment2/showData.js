module.exports = function(db, city, res) {
    db.ref("/city/" + city).once("value").then(function(snapshot) {
         res.send(snapshot.val());
         res.end();
    });

}